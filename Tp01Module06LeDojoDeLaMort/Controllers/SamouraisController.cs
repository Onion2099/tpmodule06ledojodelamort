﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BO;
using Tp01Module06LeDojoDeLaMort.Data;
using Tp01Module06LeDojoDeLaMort.Models;

namespace Tp01Module06LeDojoDeLaMort.Controllers
{
    // Controller généré avec le Wizard puis adapté
    public class SamouraisController : Controller
    {
        private Context db = new Context();

        // GET: Samourais
        public ActionResult Index()
        {
            return View(db.Samourais.ToList());
        }

        // GET: Samourais/Details/id
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Samourai samourai = db.Samourais.Find(id);
            if (samourai == null)
            {
                return HttpNotFound();
            }

            // stock dans le viewbag le potentiel du samouraï
            ViewBag.Potentiel = GetPotentiel(samourai);

            return View(samourai);
        }

        // GET: Samourais/Create
        public ActionResult Create()
        {
            var samouraiGestionVM = new SamouraiGestionVM();
            samouraiGestionVM.Armes = db.Armes.ToList();
            samouraiGestionVM.ArtMartials = db.ArtMartials.ToList();

            return View(samouraiGestionVM);
        }

        // POST: Samourais/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SamouraiGestionVM samouraiGestionVM)
        {
            if (ModelState.IsValid)
            {
                if (samouraiGestionVM.IdArmeDisponible.HasValue)
                {
                    samouraiGestionVM.Samourai.Arme = db.Armes.FirstOrDefault(a => a.Id == samouraiGestionVM.IdArmeDisponible.Value);
                }

                if (samouraiGestionVM.IdArtMartialDisponible.Count > 0)
                {
                    samouraiGestionVM.Samourai.ArtMartials = db.ArtMartials.Where(a => samouraiGestionVM.IdArtMartialDisponible.Contains(a.Id)).ToList();
                }

                db.Samourais.Add(samouraiGestionVM.Samourai);
                // Pour enregistrer tous les changements dans la bdd
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            samouraiGestionVM.Armes = db.Armes.ToList();
            return View(samouraiGestionVM);
        }

        // GET: Samourais/Edit/id
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SamouraiGestionVM samouraiGestionVM = new SamouraiGestionVM();
            samouraiGestionVM.Samourai = db.Samourais.Find(id);
            if (samouraiGestionVM.Samourai == null)
            {
                return HttpNotFound();
            }

            List<int> armeIds = db.Samourais.Where(x => x.Arme != null && x.Id != id).Select(x => x.Arme.Id).ToList();
            samouraiGestionVM.Armes = db.Armes.Where(x => !armeIds.Contains(x.Id)).ToList();

            samouraiGestionVM.ArtMartials.Add(new ArtMartial() { Nom = "Aucun" });
            samouraiGestionVM.ArtMartials.AddRange(db.ArtMartials.ToList());
            samouraiGestionVM.IdArtMartialDisponible = samouraiGestionVM.Samourai.ArtMartials.Select(x => x.Id).ToList();

            return View(samouraiGestionVM);
        }

        // POST: Samourais/Edit/id
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SamouraiGestionVM vm)
        {
            if (ModelState.IsValid)
            {
                var currentSamourai = db.Samourais.Find(vm.Samourai.Id);
                currentSamourai.Force = vm.Samourai.Force;
                currentSamourai.Nom = vm.Samourai.Nom;

                if (vm.IdArmeDisponible != null)
                {
                    var samouraisAvecMonArme = db.Samourais.Where(x => x.Arme.Id == vm.IdArmeDisponible).ToList();

                    Arme arme = null;
                    foreach (var item in samouraisAvecMonArme)
                    {
                        arme = item.Arme;
                        item.Arme = null;
                        // EntityState.Modified > Pour effectuer un chargement explicite.
                        // accès à toutes les informations dont dispose DbContext sur une entité.
                        db.Entry(item).State = EntityState.Modified;
                    }

                    if (arme == null)
                    {
                        currentSamourai.Arme = db.Armes.FirstOrDefault(x => x.Id == vm.IdArmeDisponible);
                    }
                    else
                    {
                        currentSamourai.Arme = arme;
                    }
                }
                else
                {
                    currentSamourai.Arme = null;
                }
                
                foreach (var item in currentSamourai.ArtMartials)
                {
                    db.Entry(item).State = EntityState.Modified;
                }
                currentSamourai.ArtMartials = db.ArtMartials.Where(x => vm.IdArtMartialDisponible.Contains(x.Id)).ToList();

                db.Entry(currentSamourai).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            List<int> armeIds = db.Samourais.Where(x => x.Arme != null && x.Id != vm.Samourai.Id).Select(x => x.Arme.Id).ToList();
            vm.Armes = db.Armes.Where(x => !armeIds.Contains(x.Id)).ToList();
            if (vm.Samourai.Arme != null)
            {
                vm.IdArmeDisponible = vm.Samourai.Arme.Id;
            }

            vm.ArtMartials.Add(new ArtMartial() { Nom = "Aucun" });
            vm.ArtMartials.AddRange(db.ArtMartials.ToList());
            vm.IdArtMartialDisponible = vm.Samourai.ArtMartials.Select(x => x.Id).ToList();

            return View(vm);
        }

        // GET: Samourais/Delete/id
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Samourai samourai = db.Samourais.Find(id);

            if (samourai == null)
            {
                return HttpNotFound();
            }
            ViewBag.Potentiel = GetPotentiel(samourai);

            return View(samourai);
        }

        // POST: Samourais/Delete/id
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Samourai samourai = db.Samourais.Find(id);
            db.Samourais.Remove(samourai);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        // Libérer les ressources non managées utilisées par l'appli
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // Calcul du potentiel du Samourai
        public int GetPotentiel(Samourai samourai)
        {
            int armeDegat = 0;
            int artMartial = 0;
            if (samourai.Arme != null)
            {
                armeDegat = samourai.Arme.Degats;
            }
            if (samourai.ArtMartials != null)
            {
                artMartial = samourai.ArtMartials.Count();
            }
            return (samourai.Force + armeDegat) * (artMartial + 1);
        }
    }
}
