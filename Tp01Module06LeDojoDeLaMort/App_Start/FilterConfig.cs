﻿using System.Web;
using System.Web.Mvc;

namespace Tp01Module06LeDojoDeLaMort
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
