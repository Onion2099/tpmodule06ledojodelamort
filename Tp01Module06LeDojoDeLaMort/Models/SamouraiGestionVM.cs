﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BO;

namespace Tp01Module06LeDojoDeLaMort.Models
{
    public class SamouraiGestionVM
    {
        public Samourai Samourai { get; set; }

        [Display(Name = "Arme")]
        public int? IdArmeDisponible { get; set; }
        public List<Arme> Armes { get; set; }

        public List<ArtMartial> ArtMartials { get; set; } = new List<ArtMartial>();
        public List<int> IdArtMartialDisponible { get; set; } = new List<int>();
        
    }
}