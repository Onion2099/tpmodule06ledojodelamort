﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO
{
    // Héritage TPC et TPH offrent de meilleures performances
    // Source : https://docs.microsoft.com/fr-fr/aspnet/mvc/overview/getting-started/getting-started-with-ef-using-mvc/implementing-inheritance-with-the-entity-framework-in-an-asp-net-mvc-application
    // revoir convention nommage Abstract Class > prefixe Base
    public abstract class BaseClasseId
    {
        public int Id { get; set; }
    }
}
