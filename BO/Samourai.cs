﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BO
{
    public class Samourai : BaseClasseId
    {
        [Required(ErrorMessage = "La Force du Samourai est requise.")]
        [Range(0, 99, ErrorMessage = "La Force du Samourai ne peut pas excéder 99.")]
        public int Force { get; set; }

        [Required(ErrorMessage = "Le Nom du Samourai est requis.")]
        [MinLength(1), MaxLength(30, ErrorMessage = "Le Nom du Samourai ne peut pas excéder 30 caractères.")]
        public string Nom { get; set; }
        
        public virtual Arme Arme { get; set; } // Virtual pour le Lazy loading

        [DisplayName("Arts martiaux maitrisés")]
        public virtual List<ArtMartial> ArtMartials { get; set; }
        public virtual ArtMartial ArtMartial { get; set; } // Virtual pour le Lazy loading

    }
}
