﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO
{
    public class ArtMartial : BaseClasseId
    {
        [Required(ErrorMessage = "Le Nom l'Art Martial est requis.")]
        [MinLength(1), MaxLength(30, ErrorMessage = "Le Nom de l'Art Martial ne peut pas excéder 30 caractères.")]
        public string Nom { get; set; }
    }
}
