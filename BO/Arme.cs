﻿using System.ComponentModel.DataAnnotations;

namespace BO
{
    public class Arme : BaseClasseId
    {
        [Required(ErrorMessage = "Le Nom l'Arme est requis.")]
        [MinLength(1), MaxLength(30, ErrorMessage = "Le Nom de l'Arme ne peut pas excéder 30 caractères.")]
        public string Nom { get; set; }

        [Range(0, 99, ErrorMessage = "Les Degats de l'Arme ne peuvent pas excéder 99.")]
        public int Degats { get; set; }
    }
}